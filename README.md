# 小志小视界-Bilibili资料存放

#### 介绍
- 本仓库主要存放用于哔哩哔哩账号：**小志小视界**所发布的各种视频的相关资料
- 小志小视界账号主要发布关于Stata基础学习和科研学习有关的视频
- 如何从码云平台下载资料：[科研基础-码云平台文件下载：以profile do文件为例](https://www.bilibili.com/video/BV1iy4y1z7Pv/)

#### Bilibili（哔哩哔哩）账号：
- 可直接在官网或App搜索**小志小视界**即可
- 也可以点击[小志小视界](https://space.bilibili.com/482149859)

### 代表作品(点击标题即可)
- [Stata小白的实证论文取经之路（基础篇）](https://www.bilibili.com/video/BV1HN411X7ry/)
- [综合练习-实证研究的Stata实现，毕业论文不用愁！](https://www.bilibili.com/video/BV1AD4y1d7s6)
- [连享会直播-Stata小白的取经之路：Stata学习分享从入门到进阶](https://www.bilibili.com/video/BV1ED4y1R7et)
- [Stata进阶-高效运用搜索解决计量和软件操作难题？](https://www.bilibili.com/video/BV1K54y1k7ej)
- [Stata设置-如何快速安装3000+外部命令（连玉君老师PLUS文件）](https://www.bilibili.com/video/BV1Si4y1s7K6/)

 
### 1.Stata小白的实证论文取经之路（基础篇）

#### 只学三个
- [综合练习-实证研究的Stata实现，毕业论文不用愁！](https://www.bilibili.com/video/BV1AD4y1d7s6/)
- [Stata进阶-高效运用搜索解决计量和软件操作难题？](https://www.bilibili.com/video/BV1K54y1k7ej/)
- [连享会直播-Stata学习分享从入门到进阶](https://www.bilibili.com/video/BV1ED4y1R7et/)

#### Stata软件
- [Stata基础-Stata界面简介&可重复性研究](https://www.bilibili.com/video/BV1hz411e7RL/)
- [Stata设置-中英文转换与界面背景调试](https://www.bilibili.com/video/BV1mD4y1Q7qQ/)
- [Stata基础-如何快速搞懂Stata命令和语法](https://www.bilibili.com/video/BV145411W7kM/)
- [Stata基础-do文档中代码注释的三种姿势](https://www.bilibili.com/video/BV1Qk4y1m7wM/)
- [Stata基础-代数，逻辑和关系运算符](https://www.bilibili.com/video/BV1Kv411r7MH/)
- [Stata基础-外部命令的安装与使用](https://www.bilibili.com/video/BV1Tr4y1A71j/)
- [Stata基础-系统变量_n和_N](https://www.bilibili.com/video/BV1KV411Y72c/)
- [Stata基础-解读Stata内存中的返回值/留存值](https://www.bilibili.com/video/BV1gV41127TM/)
- [Stata设置-如何正确设置连玉君老师的profile.do文件](https://www.bilibili.com/video/BV1yT4y1E7KQ/)
- [科研基础-码云平台文件下载：以profile do文件为例](https://www.bilibili.com/video/BV1iy4y1z7Pv/)
- [Stata设置-再谈Profile do文档](https://www.bilibili.com/video/BV14u41117mF/)
- [Stata设置-如何快速安装3000+外部命令（连玉君老师PLUS文件）](https://www.bilibili.com/video/BV1Si4y1s7K6/)
- [Stata设置-Stata中tmp格式缓存文件路径的确定与修改](https://www.bilibili.com/video/BV1bL4y147yj/)
- [资源分享-Stata快速入门小册子](https://www.bilibili.com/video/BV1yz4y1C7Hp/)

#### 数据专题
- [数据专题-国泰安（CSMAR）数据库使用简介与数据下载演示](https://www.bilibili.com/video/BV1bK411J7fV/)
- [Stata基础-如何从CSMAR下载数据中快速提取年份数据](https://www.bilibili.com/video/BV1u64y1c7cX/)
- [Stata基础-如何一句话给CSMAR数据变量加标签？](https://www.bilibili.com/video/BV1z64y1h7fp/)
- [数据专题-CSMAR分板块/分行业/分地区股票代码选择](https://www.bilibili.com/video/BV16r4y1y7TY/)

#### 数据导入
- [如何将数据从Excel中导入到Stata？](https://www.bilibili.com/video/BV18A411i77s/)
- [Stata基础-SPSS和SAS格式数据文件导入](https://www.bilibili.com/video/BV1ad4y1K7o8)
- [Stata进阶-如何将市场化指数正确导入Stata及转换为面板](https://www.bilibili.com/video/BV1mi4y1g7p3/)
- [Stata进阶-如何批量导入数据（以Excel数据为例）](https://www.bilibili.com/video/BV1Ha4y1H7XM/)
- [Stata基础-如何批量导入Excel的多张表单(Sheet)](https://www.bilibili.com/video/BV1st4y1k7VW/)

#### 基础命令
- [Stata基础-如何产生一个新变量-gen & egen](https://www.bilibili.com/video/BV1V64y1F78k/)
- [Stata基础-虚拟变量的生成](https://www.bilibili.com/video/BV1Eh411R7f6/)
- [Stata基础-i.variable与虚拟变量陷阱](https://www.bilibili.com/video/BV1kt4y1i7G6/)
- [Stata基础-Split分割函数:以日期型数据和高管学术经历为例](https://www.bilibili.com/video/BV1Wh411f7mb/)
- [Stata基础-字符串处理函数小结](https://www.bilibili.com/video/BV1Tp4y1Y7tr/)
- [Stata基础-分组执行命令(bysort)](https://www.bilibili.com/video/BV1iy4y1h7ty/)
- [Stata基础-group函数:分组连续编号](https://www.bilibili.com/video/BV1ny4y1H7Fn/)
- [Stata基础-组内交叉合并(joinby)](https://www.bilibili.com/video/BV1XU4y147Q8/)
- [Stata基础-暂元-暂时性的存储单元(local&global)](https://www.bilibili.com/video/BV1Si4y1g7uX/)
- [Stata基础-循环-完成重复性任务（forvalues&foreach）](https://www.bilibili.com/video/BV1aC4y1t7Jg/)

#### 数据处理
- [Stata基础-数据窗口"红黑蓝"，何种数据类型？](https://www.bilibili.com/video/BV1rt4y1U7Zg/)
- [Stata基础-蓝色数据转换黑色数据](https://www.bilibili.com/video/BV1MK411c75i/)
- [Stata基础-如何快速处理数据中的日期类型变量](https://www.bilibili.com/video/BV1Gv411i7gQ/)
- [Stata基础-如何快速删除样本中的重复值？](https://www.bilibili.com/video/BV1Kp4y1v7XK/)
- [Stata基础-"重复值"处理，不要误伤"自己人"](https://www.bilibili.com/video/BV16541187b7/)
- [Stata基础-如何处理数据中的极端值或异常值？](https://www.bilibili.com/video/BV1yt4y1U73m/)
- [Stata基础-如何处理样本中的缺失值](https://www.bilibili.com/video/BV1yT4y1L7UD/)
- [Stata基础-如何进行数据横向合并与纵向追加](https://www.bilibili.com/video/BV1qT4y1w7sX/)
- [Stata基础-省级层面和公司层面数据合并：以GDP为例](https://www.bilibili.com/video/BV1nW4y1U71g)
- [Stata基础-如何剔除金融行业数据](https://www.bilibili.com/video/BV1VY4y1z73n)


#### 变量构建
- [Stata实操-高管个人经历的关键变量构建](https://www.bilibili.com/video/BV1Ky4y1q7aU/)
- 需要更多的视频

#### 实证部分
- [Stata基础-描述性统计结果输出及解读运用](https://www.bilibili.com/video/BV1L54y1m7pW/)
- [Stata基础-相关性分析结果输出与解读运用](https://www.bilibili.com/video/BV1qA411E7ZN/)
- [Stata基础-单变量差异检验结果输出与解读运用](https://www.bilibili.com/video/BV1L5411877n/)
- [Stata基础-单变量差异检验差异解读与表格输出](https://www.bilibili.com/video/BV1f3411g7SU)
- [Stata基础-如何选择合适的回归命令？](https://www.bilibili.com/video/BV1Uz4y1d7Ux/)
- [Stata基础-如何在回归中控制行业效应](https://www.bilibili.com/video/BV1DV411m7um/)
- [Stata基础-如何快速输出Stata回归结果？](https://www.bilibili.com/video/BV1i5411j7Ef/)
- [Stata基础-截面数据Stata回归结果解读](https://www.bilibili.com/video/BV1YT4y1c7eM/)
- [Stata基础-回归结果中R2报告](https://www.bilibili.com/video/BV1Ut4y1s72F)
- [Stata基础-回归结果R2的输出](https://www.bilibili.com/video/BV1Qr4y1t7LR)
- [Stata基础-如何对截面数据分组](https://www.bilibili.com/video/BV1wV411Y7NW/)
- [Stata基础-交互效应和分组回归（程序实现）](https://www.bilibili.com/video/BV1pK411N7bt/)
- [Stata基础-东部、中部、西部的分组回归](https://www.bilibili.com/video/BV1tG411s7Hf)
- [Stata基础-稳健性检验！！！](https://www.bilibili.com/video/BV14a411c7mR/)

#### 面板数据
- [Stata基础-截面数据&面板数据（概念篇）](https://www.bilibili.com/video/BV1za411A7kb/)
- [Stata基础-截面数据&面板数据（方法篇）](https://www.bilibili.com/video/BV1Wi4y177sg/)
- [Stata基础-如何正确设定面板数据](https://www.bilibili.com/video/BV1LT4y1F7yC/)
- [Stata基础-面板数据模式解析(xtpattern)](https://www.bilibili.com/video/BV1Tv41147H4/)
- [Stata基础-固定效应模型](https://www.bilibili.com/video/BV1zZ4y1g79q/)
- [Stata基础-个体、时间、行业、省份的固定效应](https://www.bilibili.com/video/BV1Y44y137ew)
- [Stata基础-控制省份固定效应](https://www.bilibili.com/video/BV1VA4y1d7Jg)
- [Stata基础-固定效应命令：xtreg、areg、reghdfe](https://www.bilibili.com/video/BV18r4y187db)
- [Stata基础-如何输出行业和时间固定效应](https://www.bilibili.com/video/BV1mS4y1i7ou)
- [Stata基础-聚类稳健标准误(Robust)](https://www.bilibili.com/video/BV1Yz4y1r7Yd/)

#### 进阶学习
- [如何快速获取经济金融权威期刊论文的数据和程序](https://www.bilibili.com/video/BV1c5411e7aZ/)
- [学习资源分享-计量经济大佬喊你快来免费学习！！！](https://www.bilibili.com/video/BV1P54y1q7XA/)

### 2.实证经典论文研读（计量方法篇）
#### 双重差分法（DID）
- [放松银行管制与国家收入分配差距（引言）](https://www.bilibili.com/video/BV1Rq4y1Y7eV/)
- [放松银行管制与国家收入分配差距（数据与方法）](https://www.bilibili.com/video/BV17a41147sv/)
- [放松银行管制与国家收入分配差距（实证基础结果）](https://www.bilibili.com/video/BV1Zr4y1H7MR/)
- [放松银行管制与国家收入分配差距（影响渠道分析）](https://www.bilibili.com/video/BV1FY4y1B7LV/)
- [放松银行管制与国家收入分配差距（表格结果复现）](https://www.bilibili.com/video/BV13P4y1K7wx/)
- [放松银行管制与国家收入分配差距（图形结果复现）](https://www.bilibili.com/video/BV1xY41177S9/)
- [QJE土豆的力量：土豆引种、人口增长和城市化（引言）](https://www.bilibili.com/video/BV1UP4y1K7dL/)
- [QJE土豆的力量：土豆引种、人口增长和城市化（研究背景）](https://www.bilibili.com/video/BV1Cr4y1p7zn/)
- [QJE土豆的力量：土豆引种、人口增长和城市化（概念框架，数据与实证策略）](https://www.bilibili.com/video/BV1t3411J7Ji/)
- [QJE土豆的力量：土豆引种、人口增长和城市化（灵活估计，滚动回归与基准回归）](https://www.bilibili.com/video/BV1r44y1V7Vg/)
- [QJE土豆的力量：土豆引种、人口增长和城市化（稳健性检验与结论）](https://www.bilibili.com/video/BV1ir4y1n7kj/)
- [QJE土豆的力量：土豆引种、人口增长和城市化（主要结果复现）](https://www.bilibili.com/video/BV1cY4y1p7cL)
- [QJE土豆的力量：土豆引种、人口增长和城市化（稳健性结果复现）](https://www.bilibili.com/video/BV1DY4y1e7e1)
- [享受宁静的生活：公司治理与管理者偏好（引言）](https://www.bilibili.com/video/BV1Ni4y1S7YN)
- [享受宁静的生活：公司治理与管理者偏好（数据与实证策略）](https://www.bilibili.com/video/BV1hu411k7Uf)
- [享受宁静的生活：公司治理与管理者偏好（实证结果解读1）](https://www.bilibili.com/video/BV1g541117V9)
- [享受宁静的生活：公司治理与管理者偏好（稳健性与结论）](https://www.bilibili.com/video/BV17A4y1X7GP)

### 3.研究生科研基础相关视频
#### 科研基础
- [科研基础-中文学术期刊分类（C刊&C扩&南核&北核）](https://www.bilibili.com/video/BV1wT41177x5)
- [科研基础-英文学术期刊分类（SSCI&UTD24&FT50&ABS）](https://www.bilibili.com/video/BV1BB4y1b7a5)
- [科研基础-中英文期刊等级查询（场景篇）](https://www.bilibili.com/video/BV1ie4y1D77h)
- [科研基础-文献检索三部曲（序章）](https://www.bilibili.com/video/BV19i4y1C7qD/)
- [科研基础-文献检索三部曲（中篇）](https://www.bilibili.com/video/BV1PR4y1F7Mw/)
- [科研学习-文献检索三部曲（终章）](https://www.bilibili.com/video/BV1w5411S7s5)
- [科研基础-不容错过的经济金融会计期刊](https://www.bilibili.com/video/BV1ii4y177Hg/)
- [科研基础-如何从知网高效检索研究领域的重要文献](https://www.bilibili.com/video/BV18Z4y1H7Mi/)
- [科研基础-如何从Web of Science聚合检索研究话题的权威文献](https://www.bilibili.com/video/BV1Ak4y117jx/)
- [科研基础-经管科研人都在关注的公众号](https://www.bilibili.com/video/BV1JZ4y1k7G7/)
- [科研基础-怎样好好的提问](https://www.bilibili.com/video/BV1Qr4y1z7kc/)
- [科研基础-如何快速搜索实证问题](https://www.bilibili.com/video/BV1NT4y1X7Dg/)
- [科研基础-如何做好中英文参考文献](https://www.bilibili.com/video/BV1Xi4y1C7Ua/)
- [科研基础-知网硕博论文下载：拒绝CAJ，下载PDF](https://www.bilibili.com/video/BV1a44y1u7Aq)
- [科研基础-英文文献高效下载神器Sci-hub](https://www.bilibili.com/video/BV1Wy4y1E7d9/)
- [科研工具-如何快速上手EV录屏软件](https://www.bilibili.com/video/BV1vS4y1L7P5/)
- [资源推荐-《因果推断实用计量方法》](https://www.bilibili.com/video/BV1uy4y187Zo/)

#### 申请考核
- [硕士/博士申请考核-研究计划or研究设计怎么撰写？](https://www.bilibili.com/video/BV1PA411w7uQ/)
- [硕士/博士申请考核-如何找到心仪的导师？](https://www.bilibili.com/video/BV16w411f76b/)
- [硕士/博士申请考核-如何写邮件联系意向老师？](https://www.bilibili.com/video/BV1KQ4y1k778/)
- [硕士/博士申请考核-如何撰写个人简历？](https://www.bilibili.com/video/BV17Q4y1z7wE/)

#### 文献选读
- [文献选读-劳动力失业风险与公司融资决策（JFE2013）](https://www.bilibili.com/video/BV1b34y1m75B/)
- [文献选读-工会，集体议价与企业资本结构（JF2010）](https://www.bilibili.com/video/BV1CT4y1o7k2/)
